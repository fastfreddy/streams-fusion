Streams-fusion readme
---------------------


What is this?
-------------

[Streams-fusion](https://fastfreddy.gitlab.io/streams-fusion/) is an addon to the [Streams](https://saqimtiaz.github.io/streams/) plugin that lets the user fuse (merge) a hierarchy of nodes from a stream, into a single tiddler's wikitext.

Features
--------

*   preserve the streams node hierarchy
*   supports list bullets or numbered bullets
*   supports sub-tree exports (using the node context menu or the keyboard shortcuts)
*   exports the root note's text if it is there
*   wraps complex nodes inside `<div>` tags, supporting hyper complex streams such as [https://saqimtiaz.github.io/streams/#FAQs](https://saqimtiaz.github.io/streams/#FAQs) and [https://saqimtiaz.github.io/streams/#Working%20with%20streams%20tiddlers](https://saqimtiaz.github.io/streams/#Working%20with%20streams%20tiddlers) well (please note that I am not using the "perfectly" adjective here)

Installation
------------

### Dependencies:

First, install these two plugins (visit the page, follow the instructions):

*   [the Streams plugin](https://saqimtiaz.github.io/streams/) (**v1.2.22** or greater)
*   [relink plugin](https://flibbles.github.io/tw5-relink/)

### Installation steps:

*   Then you can install the [streams-fusion](#%24%3A%2Fplugins%2Fphiv%2Fstreams-fusion) plugin (drag and drop into your TiddlyWiki notebook)


See the rest of the plugin's readme [here](https://fastfreddy.gitlab.io/streams-fusion/)
